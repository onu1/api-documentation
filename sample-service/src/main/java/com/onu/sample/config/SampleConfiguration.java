package com.onu.sample.config;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.onu.service.config.PasswordLookupDataSourceFactory;
import com.onu.service.config.UrlBuilder;

import io.dropwizard.Configuration;

public class SampleConfiguration extends Configuration {
    @Valid
    @NotNull
    @JsonProperty("database")
    private PasswordLookupDataSourceFactory database = new PasswordLookupDataSourceFactory();

    @Valid
    @NotNull
    private UrlBuilder apiUrl = new UrlBuilder();

    public UrlBuilder getApiUrl() {
        return apiUrl;
    }
   
    public PasswordLookupDataSourceFactory getDataSourceFactory() {
        return database;
    }

}
