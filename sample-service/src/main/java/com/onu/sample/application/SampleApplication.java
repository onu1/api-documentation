package com.onu.sample.application;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration.Dynamic;

import org.eclipse.jetty.servlets.CrossOriginFilter;

import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module.Feature;
import com.onu.sample.config.SampleConfiguration;

import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.ScanningHibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class SampleApplication extends Application<SampleConfiguration> {

	@Override
	public void initialize(Bootstrap<SampleConfiguration> serviceConfigBootstrap) {
		serviceConfigBootstrap.addBundle(migrationsBundle);
		serviceConfigBootstrap.addBundle(hibernateBundle);
	}

	@Override
	public void run(SampleConfiguration configuration, Environment environment) throws Exception {
		configureCors(environment);
	}

	private final MigrationsBundle<SampleConfiguration> migrationsBundle = new MigrationsBundle<SampleConfiguration>() {
		public DataSourceFactory getDataSourceFactory(SampleConfiguration configuration) {
			return configuration.getDataSourceFactory();
		}
	};

	private final ScanningHibernateBundle<SampleConfiguration> hibernateBundle = new ScanningHibernateBundle<SampleConfiguration>(
			"com.onu.faksion.core") {
		public DataSourceFactory getDataSourceFactory(SampleConfiguration configuration) {
			return configuration.getDataSourceFactory();
		}

		@Override
		protected Hibernate4Module createHibernate4Module() {
			return new Hibernate4Module().disable(Feature.USE_TRANSIENT_ANNOTATION);
		}
	};

	private void configureCors(Environment environment) {
		Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
		filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
		filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
		filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
		filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
		filter.setInitParameter("allowedHeaders",
				"x-project-id,Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
		filter.setInitParameter("allowCredentials", "true");
	}

	public static void main(String[] args) throws Exception {
        new SampleApplication().run(args);
    }

}
